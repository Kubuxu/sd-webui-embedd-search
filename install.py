import launch
import os

if not launch.is_installed("chromadb"):
    launch.run_pip("install chromadb", "chromadb requirement for webui-search")
