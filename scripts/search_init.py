import os
from typing import Union

from modules import paths, shared, script_callbacks, scripts, images, devices
from modules.shared import opts
import logging

import torch
from torchvision.transforms import PILToTensor

import gradio as gr
import open_clip
from PIL import Image
import numpy as np
from itertools import islice
from tqdm.rich import tqdm

import clip_interrogator
from clip_interrogator import Config, Interrogator

import chromadb
from chromadb.config import Settings

log = logging.getLogger("search")
log.setLevel(logging.DEBUG)


class ClipLoader:
    def __init__(self):
        self.clip_model_name_pt = None
        self.model = None
        self.preprocess = None
        self.tokenize = None
        self.dtype = None

    def load_model(self, clip_model_name_pt):
        if (
            self.clip_model_name_pt is None
            or self.clip_model_name_pt != clip_model_name_pt
        ):
            clip_model_name, clip_model_pretrained_name = clip_model_name_pt.split(
                "/", 2
            )
            clip_model, _, clip_preprocess = open_clip.create_model_and_transforms(
                clip_model_name,
                pretrained=clip_model_pretrained_name,
                precision="fp32" if devices.dtype == torch.float32 else "fp16",
                device=devices.get_optimal_device(),
                jit=False,
                cache_dir=None,
            )
            clip_model.eval()
            self.clip_model_name_pt = clip_model_name_pt
            self.model = clip_model
            self.preprocess = clip_preprocess
            self.tokenize = open_clip.get_tokenizer(clip_model_name)
            self.dtype = torch.float32 if devices.dtype == torch.float32 else torch.float16


def batch_iterator(generator, batch_size):
    while True:
        batch = list(islice(generator, batch_size))
        if not batch:
            break
        yield batch


class EmbeddingSearch:
    def __init__(self, basedir):
        self.basedir = basedir
        self.__chroma = None
        self.clip = ClipLoader()
        self.query_result = None

    chroma_client_instance: Union[chromadb.Client, None] = None

    @property
    def chroma(self) -> chromadb.API:
        if self.__chroma is None:
            chroma_path = os.path.join(self.basedir, "chroma.db")
            log.info(f"Opening chromadb at {chroma_path}")
            self.__chroma = chromadb.Client(
                Settings(
                    chroma_db_impl="duckdb+parquet",
                    persist_directory=chroma_path,
                )
            )
        return self.__chroma

    def chroma_collection(self):
        return self.chroma.get_collection("imgEmbedd", embedding_function=None)

    def chroma_reset(self):
        client = self.chroma
        client.reset()
        client.create_collection("imgEmbedd", metadata={"hnsw:space": "cosine"})

    def txt2img_path(self):
        return (opts.outdir_samples or opts.outdir_txt2img_samples,)

    def get_all_files(self, directories: list[str]):
        all_files = []
        for root in directories:
            for _, _, files in os.walk(root):
                for file in files:
                    file_path = os.path.join(root, file)
                    all_files.append(file_path)
        return all_files

    """
    def is_lowvram():
        low_vram = shared.cmd_opts.lowvram or shared.cmd_opts.medvram
        if not low_vram and torch.cuda.is_available():
            device = devices.get_optimal_device()
            vram_total = torch.cuda.get_device_properties(device).total_memory
            if vram_total <= 12 * 1024 * 1024 * 1024:
                low_vram = True
        return low_vram
    """

    def image_generator(self, files):
        for filename in files:
            if filename.endswith(".jpg") or filename.endswith(".png"):
                try:
                    image = Image.open(filename)
                    yield filename, image
                except Exception as e:
                    log.warning(f"Error loading image: {filename}\n{e}")

    def reindex(self, clip_model_name_pt: str, batch_size: int):
        log.debug("Starting reindex")

        self.chroma_reset()

        files = self.get_all_files(self.txt2img_path())
        files = [file for file in files if not file.endswith(".txt")]

        self.clip.load_model(clip_model_name_pt)

        # log.warning(f"pre-process: {clip_preprocess}")
        total = 0
        for names_and_images in batch_iterator(
            self.image_generator(tqdm(files, desc="Embedding images")), batch_size
        ):
            names, images = tuple(zip(*names_and_images))

            images_processed = torch.cat(
                [self.clip.preprocess(image).unsqueeze(0) for image in images]
            ).to(device=devices.device, dtype=self.clip.dtype)

            collection = self.chroma_collection()

            with torch.no_grad(), torch.cuda.amp.autocast():
                image_embed = (
                    self.clip.model.encode_image(images_processed).to("cpu").float()
                )

                total += image_embed.shape[0]
                # We do not normalize, we let the the chroma cosine space deal with it.
                # image_embed = torch.nn.functional.normalize(image_embed)
                collection.upsert(
                    ids=list(names),
                    embeddings=image_embed.tolist(),
                )

                # log.warning(f'Tensor norm:{torch.linalg.norm(image_embed, dim=1)}')
                # image_features /= image_features.norm(dim=-1, keepdim=True)

        self.chroma.persist()
        log.info(f"Added {total} images to the index")

    def query(self, query: str, clip_model_name_pt: str, specificity: float):
        self.clip.load_model(clip_model_name_pt)
        query_tokens = self.clip.tokenize(query).to(device=devices.device)
        query_embed = self.clip.model.encode_text(query_tokens).float()
        res = self.chroma_collection().query(query_embeddings=query_embed.tolist())

        log.warning(f"res: {res}")

        self.query_result = res
        self.embedd_dim = query_embed.shape[1]
        self.count = self.chroma_collection().count()
        return self.filter(specificity)
    def filter(self, specificity):
        if self.query_result is None:
            return []
        n = self.count
        d = self.embedd_dim * np.exp(-specificity)
        
        from scipy import stats
        mu = stats.norm.ppf(1 - 1 / n, 0, np.sqrt(1 / d))
        sigma = stats.norm.ppf(1 - 1 / n * np.exp(-1), 0, np.sqrt(1 / d)) - mu
        max_distance = 1 - stats.genextreme(c=0, loc=mu, scale=sigma).ppf(0.99)
        log.info(f"Max distance: {max_distance}")


        return [
            id
            for id, distance in zip(self.query_result["ids"][0], self.query_result["distances"][0])
            if distance < max_distance
        ]

    def get_models(self):
        return ["/".join(x) for x in open_clip.list_pretrained()]

    def on_ui_tabs(self):
        log.setLevel(logging.DEBUG)
        with gr.Blocks(analytics_enabled=False) as search_block:
            with gr.Row():
                with gr.Column(scale = 2):
                    query = gr.Textbox("", label="Serch query", width='60%')
                with gr.Column(scale = 1):
                    search_button = gr.Button("Search", variant="primary")
                    specificity = gr.Slider(-2, +2, 0, label="Specificity")
            results_gallery = gr.Gallery(
                value=[],
                label="Output",
                show_label=False,
                elem_id=f"search_gallery",
            ).style(preview=False, container=False, columns=[1, 2, 3, 4, 5, 6])

            model = gr.Dropdown(
                self.get_models(),
                value="ViT-L-14/laion400m_e32",
                label="CLIP Model",
            )
            batchSize = gr.Slider(128, 1024, 256, step=16, label="Batch Size:")
            gr.Button("Re-index", elem_id=None, variant="primary").click(
                self.reindex, inputs=[model, batchSize]
            )
            search_button.click(
                self.query,
                inputs=[query, model, specificity],
                outputs=[results_gallery],
            )
            specificity.change(self.filter, inputs=[specificity], outputs=[results_gallery])
        return ((search_block, "Search", "embed_search"),)

    def register_callbacks(self):
        script_callbacks.on_ui_tabs(self.on_ui_tabs)


search = EmbeddingSearch(scripts.basedir())
search.register_callbacks()
